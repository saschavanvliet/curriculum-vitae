# Curriculum Vitae

## Check cv

Check the resume.json file against the schema. The command has to be executed in the folder which contains the resume.json file.
```
C:\cv\en> resume validate
```

## Build CV

Create the CV with the following command

```
C:\cv> resume export en\resume\cv-en.html --resume en\resume.json --format html --theme stackoverflow
```
